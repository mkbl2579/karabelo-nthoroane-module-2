void main() {
 // Sort and Print by Name
 List sortedList = Winners();
  sortedList.sort((a,b){
   return a.compareTo(b);
 });
  
  // Iterate thru list
  for(int i = 0; i < sortedList.length; i++){
    print(sortedList[i]);
  }
  
  // Print Winners
  print("Winning App of 2017: ${Winners().elementAt(37)}");
  print("Winning App of 2018: ${Winners().elementAt(47)}");
  
  // Print Count
  print("Total Apps: ${sortedList.length}");
  
  
}

// Handle List of Winners by Years
List Winners() {
  
  return [
   // List of Winners for the year 2012
      "snapscan",
      "fnb-banking",
      "healthid",
      "matchy",
      "phrazapp",
      "plascon",
      "rapid-targets",
  
    // List of Winners for the year 2013
      "zapper",
      "bookly",
      "deloitte-digital",
      "dstv",
      "gautrain-buddy",
      "kids-aid",
      "markitshare",
      "nedbank",
      "price-check",
  
  // List of Winners for the year 2014
      "live-inspect",
      "my-belongings",
      "rea-vaya",
      "supersport",
      "vigo",
      "wildlife-tracker",
  
  // List of Winners for the year 2015
      "tuta-me",
      "cput-mobile",
      "dstv-now",
      "eskomsepush",
      "m4jam",
      "vula-mobile",
      "wumdrop",

  // List of Winners for the year 2016
      "ecoslips",
      "domestly",
      "friendly-math-monsters-for-kindergarten",
      "hearza",
      "ikhokha",
      "kaching",
      "mibrand",
  
  // List of Winners for the year 2017
      "asi-snakes",
      "orderin",
      "intergreatmev",
      "pick-n-pays-super-animals",
      "watif-health-portal",
      "transunion-1check",
      "hey-jude",
      "oru-social-touchsa",
      "awethu-project",
      "zulzi",
      "shyft-for-standard-bank",

  // List of Winners for the year 2018
      "cowa-bunga",
      "acgl ",
      "besmarta ",
      "xander-english",
      "ctrl ",
      "pineapple ",
      "bestee",
      "stokfella",
  
  // List of Winners for the year 2019
      "digger",
      "si-realities",
      "vula-mobile",
      "hydra-farm",
      "matric-live",
      "franc",
      "over",
      "loctransi",
      "naked-insurance",
      "loot-defence",
      "my-pregnancy-journey",
  
  // List of Winners for the year 2020
      "easy-equities",
      "birdpro",
      "technishen",
      "matric-live",
      "bottles",
      "lexie-hearing",
      "league-of-legends",
      "examsta",
      "xitsonga-dictionary",
      "greenfingers-mobile",
      "guardian-health",

  // List of Winners for the year 2021
      "transunion-dealers-guide",
      "ambani-afrika",
      "takealot",
      "shytft",
      "iiDENTIFii",
      "sisa",
      "guardian-health-platform",
      "murimi",
      "uniWise",
      "kazi-app",
      "rekindle",
      "hellopay-softPOS",
      "roadsave"];
}
