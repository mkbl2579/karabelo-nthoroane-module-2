// Define Class
class App{
  String appName = 'Ambani Afrika';
  String appCatergory = 'Educational';
  String appDeveloper = 'Ambani Afrika';
  String appWinning_year = '2021';
    
  // Class Function
  printInfo(){
    print("${appName.toUpperCase()}");
    print("Catergory: ${appCatergory}");
    print("Developer: ${appDeveloper}");
    print("Winning Year: ${appWinning_year}");
  }
}
    
void main() {
  // Call Class
  var app = new App();
  app.printInfo();
}
