void main() {
  // Store Variables
  String name = 'Karabelo Nthoroane';
  String fav_App = 'Facebook';
  String city = 'Bloemfontein';
  
  // Generate Message
  String msg = 'Hello, My name is $name,\n'
  'I am from $city.\n'
  'My favourite app is $fav_App!';
  
  // Print Variables
  print(msg);
  
  
}
